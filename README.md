# React & Redux harjoittelu

Videosarja jota seuraan: https://app.pluralsight.com/library/courses/react-redux-react-router-es6/table-of-contents

## Setup

### Preppaus
Jos käytät Windows konetta devaamiseen, varmistu siitä, että käytät Noden 5.12.0 versiota.

Windowsilla eri Noden versioiden hallintaa varten kannattaa asentaa ["Node Version Manager"](https://github.com/coreybutler/nvm-windows) (NVM).

### Asentaminen
1. Kloonaa repo
2. Asenna projektin Node moduulit
  * `npm install`
3. Aja `npm start -s`, jolloin selain aukeaa ja Webpack tarkastelee projektin tiedostoja muutosten varalta
