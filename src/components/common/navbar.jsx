import React, { PropTypes } from "react";
import { bindActionCreators } from "redux";
import { Link } from "react-router";
import { connect } from "react-redux";

import toastr from "toastr";
import cookie from "react-cookie";
import bootstrap from "bootstrap";

import * as authActions from "../../actions/authActions";
import LoginModal from "../auth/loginModal.jsx";

class Navbar extends React.Component {

	constructor(props, context) {
		super(props, context);

		this.state = {
			auth		: props.auth,
			showModal	: false,

			credentials	: Object.assign({}, props.credentials),
			errors	    : {},
            saving      : false
		};

		this.onLogoutClick = this.onLogoutClick.bind(this);
		this.onChange = this.onChange.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
		this.modalClose = this.modalClose.bind(this);
		this.modalOpen = this.modalOpen.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		if(this.props.auth.user.id != nextProps.auth.user.id) {
			this.setState({ auth: Object.assign({}, nextProps.auth) });
		}
	}

	onChange(e) {
		const field = e.target.id;
		let credentials = this.state.credentials;
		credentials[field] = e.target.value;


		return this.setState({ credentials: credentials });
	}

    onSubmit(e) {
        if(e) e.preventDefault();

        this.setState({ saving: true });

        this.props.actions.authenticate(this.state.credentials)
            .then((response) => {
                this.setState({ saving: false });
				this.modalClose();
            })
			.catch((errors) => {
				this.setState({ saving: false });
			});
    }


	modalClose(e) {
		if(e) e.preventDefault();

		this.setState({ showModal: false });
	}

	modalOpen(e) {
		if(e) e.preventDefault();

		this.setState({ showModal: true });
	}

	onLogoutClick(e) {
		if(e) e.preventDefault();

		this.props.onLogoutClick();
	}

	adminNav() {
		if(this.state.auth.authenticated === true) {
			// Logged in
			return(
				<ul className="nav navbar-nav">
					<li className="dropdown">

						<a 	className="dropdown-toggle"
							data-toggle="dropdown"
							role="button"
							aria-haspopup="true"
							aria-expanded="false">Mikrobiologit <span className="caret"></span></a>

						<ul className="dropdown-menu">
							<li><Link to="/kmb/articles" 	activeClassName="active">Artikkelit</Link></li>
							<li><Link to="/kmb/users" 		activeClassName="active">Käyttäjät</Link></li>
							<li role="separator" className="divider"></li>
							<li><a href="https://kliinisetmikrobiologit.ddns.net/" target="_blank">Sivusto</a></li>
						</ul>

					</li>
				</ul>
			);
		} else {
			// Logged out
			return(
				""
			);
		}
	}

	authButtons() {
		if(this.state.auth.authenticated === true) {
			// Logged in
			return(
				<a 	className="btn btn-sm navbar-btn btn-danger"
					id="logout"
					onClick={this.onLogoutClick}
				>
					Kirjaudu ulos
				</a>
			);
		} else {
			// Logged out
			return(
				<a className="btn btn-sm navbar-btn btn-primary"
					id="kirjaudu"
					onClick={this.modalOpen}
				>
					Kirjaudu
				</a>
			);
		}
	}

	render() {
		return (
			<nav className="navbar navbar-default">

				<div className="container-fluid">
					<div className="navbar-header">
						<button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false">
							<span className="sr-only">Toggle navigation</span>
							<span className="icon-bar"></span>
							<span className="icon-bar"></span>
							<span className="icon-bar"></span>
						</button>
						<Link to="/" className="navbar-brand">Mits Admin Panel</Link>
					</div>

					<div className="collapse navbar-collapse" id="navbar">

						{this.adminNav()}

						<ul className="nav navbar-nav navbar-right">

							{this.authButtons()}

						</ul>
					</div>
				</div>

				<LoginModal
					onChange={this.onChange}
					onSubmit={this.onSubmit}
					creds={this.state.credentials}
					saving={this.state.saving}
					showModal={this.state.showModal}
					close={this.modalClose}
				/>

			</nav>
		);
	}
}

Navbar.propTypes = {
	auth			: PropTypes.object.isRequired,
	onLogoutClick	: PropTypes.func.isRequired,

	credentials 	: PropTypes.object.isRequired,
	actions     	: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
	let credentials = {
		email       : "",
		password    : ""
	};

	return {
		auth		: state.auth,
		credentials	: credentials
	};
}

function mapDispatchToProps(dispatch) {
	return {
		onLogoutClick: () => {
			dispatch(authActions.logout());
		},

		actions: bindActionCreators(authActions, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Navbar);
