import React, { Component, PropTypes } from "react";
import ReactSummernote from "react-summernote";
import "react-summernote/dist/react-summernote.css";
import "react-summernote/lang/summernote-fi-FI";
 
import "bootstrap/js/modal";
import "bootstrap/js/dropdown";
import "bootstrap/js/tooltip";
import "bootstrap/dist/css/bootstrap.css";
 
class RichTextEditor extends Component {

	constructor(props, context) {
		super(props, context);

		this.onChange = this.onChange.bind(this);
	}

	onChange(content) {

		/**
		 * Kutsutaan parentin (manageArticlePage) onChange
		 * 
		 * Params
		 * Object: event
		 * String: target
		 * String: content
		 * 
		 * Return
		 * Object: Updated article
		 * 
		 */
		return this.props.onChange(null, "body", content);

	}

	render() {
		return (
			<ReactSummernote
				value={this.props.article.body}
				options={{
					lang: "fi-FI",
					height: 350,
					dialogsInBody: true,
					toolbar: [
						["style", ["style"]],
						["font", ["bold", "underline", "clear"]],
						["fontname", ["fontname"]],
						["para", ["ul", "ol", "paragraph"]],
						["table", ["table"]],
						["insert", ["link", "picture", "video"]],
						["view", ["fullscreen", "codeview"]]
					]
				}}
				onChange={this.onChange}
			/>
		);
	}
}

RichTextEditor.propTypes = {
	article: PropTypes.object,
	onChange: PropTypes.func
};

export default RichTextEditor;