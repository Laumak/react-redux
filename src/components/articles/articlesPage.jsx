import React, { PropTypes } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { orderBy } from "lodash";

import * as ArticleActions from "../../actions/articleActions";

import ArticlesList from "./articlesList.jsx";
import { browserHistory } from "react-router";

class ArticlesPage extends React.Component {

	constructor(props, context) {
		super(props, context);

		this.redirectToAddArticlePage = this.redirectToAddArticlePage.bind(this);
	}

	articleRow(article, index) {
		return <div key={index}>{article.title}</div>;
	}

	redirectToAddArticlePage() {
		browserHistory.push("/kmb/article");
	}

	render() {
		const articles = this.props.articles;
		const sortedArticles = orderBy(articles, ["updated_at", "title"], ["desc", "asc"]);

		return (
			<div>

				<div className="panel panel-default">
					<div className="panel-heading">Artikkelit</div>
					<div className="panel-body clearfix">

						<ArticlesList articles={sortedArticles} />

						<input 	type="submit"
								value="Luo artikkeli"
								className="btn btn-success pull-right"
								onClick={this.redirectToAddArticlePage}
						/>

					</div>
				</div>
				
			</div>
		);
	}

}

ArticlesPage.propTypes = {
	articles: PropTypes.array.isRequired,
	actions: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
	return {
		// Ks. RootReducerin (reducers/index.js) combineReducers funktion sisältö.
		articles: state.articles
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(ArticleActions, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(ArticlesPage);
