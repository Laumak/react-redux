import React from "react";

import Textarea from "../common/textarea.jsx";
import TextInput from "../common/textInput.jsx";
import Summernote from "../common/summernote.jsx";
import SelectInput from "../common/selectInput.jsx";

const ArticleForm = ({article, onSave, onChange, saving, errors}) => {
	return (
		<form>
			
			<TextInput
				name="title"
				label="Otsikko"
				value={article.title}
				onChange={onChange}
				error={errors.title}
			/>

			<TextInput
				name="slug"
				label="Slug"
				value={article.slug}
				onChange={onChange}
				error={errors.slug}
			/>

			<Summernote
				name="body"
				label="Sisältö"
				placeholder="Kirjoita..."
				article={article}
				onChange={onChange}
				error={errors.body}
			/>

			<div className="form-group">
				<button className="btn btn-success" onClick={onSave}>
					Tallenna {saving ? <i className="fa fa-spinner fa-pulse fa-fw"></i> : ""}
				</button>
			</div>
			
		</form>
	);
};

ArticleForm.propTypes = {
	article: React.PropTypes.object.isRequired,
	onSave: React.PropTypes.func.isRequired,
	onChange: React.PropTypes.func.isRequired,
	saving: React.PropTypes.bool,
	errors: React.PropTypes.object
};

export default ArticleForm;
