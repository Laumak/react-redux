import React, { PropTypes } from "react";

import ArticlesListRow from "./articlesListRow.jsx";

const ArticleList = ({ articles }) => {

	return (
		<table className="table table-striped">

			<thead>
				<tr>
					<th>Otsikko</th>
					<th>Slug</th>
				</tr>
			</thead>
			
			<tbody>
				{articles.map(article =>
					<ArticlesListRow key={article.id} article={article} />
				)}
			</tbody>

		</table>
	);
};

ArticleList.propTypes = {
    articles: PropTypes.array.isRequired
};

export default ArticleList;
