import React, { PropTypes } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { forEach } from "lodash";
import toastr from "toastr";

import ArticleForm from "./articleForm.jsx";
import * as ArticleActions from "../../actions/articleActions";

class ManageArticlePage extends React.Component {

	constructor(props, context) {
		super(props, context);

		this.state = {
			article	: Object.assign({}, props.article),
			errors	: {},
			saving	: false
		};

		this.updateArticleState	= this.updateArticleState.bind(this);
		this.saveArticle		= this.saveArticle.bind(this);
	}

	componentWillReceiveProps(nextProps) {
		if(this.props.article.id != nextProps.article.id) {
			this.setState({ article: Object.assign({}, nextProps.article) });
		}
	}

	updateArticleState(event, target, content) {

		let field = "";

		let article = this.state.article;

		if(event) {
			field = event.target.name;
			article[field] = event.target.value;
		} else {
			article[target] = content;
		}

		return this.setState({ article: article });
		
	}

	saveArticle(event) {

		event.preventDefault();

		this.setState({ saving: true });

		this.props.actions.saveArticle(this.state.article)
			.then(() => this.redirect())
			.catch((error) => {
				let errorArr = error.response.body.error.errors;

				forEach(errorArr, function(item, index, list) {
					toastr.error(item);
				});

				this.setState({ saving: false });
			});

	}

	redirect() {
		this.setState({ saving: false });
		this.context.router.push("/kmb/articles");
	}

	render() {
		return(

			<div className="panel panel-default">
				<div className="panel-heading">Muokkaa artikkelia</div>
				<div className="panel-body">

					<ArticleForm
						onChange={this.updateArticleState}
						onSave={this.saveArticle}
						article={this.state.article}
						errors={this.state.errors}
						saving={this.state.saving}
					/>

				</div>
			</div>

		);
	}

}

ManageArticlePage.propTypes = {
	article: PropTypes.object.isRequired,
	actions: PropTypes.object.isRequired
};

ManageArticlePage.contextTypes = {
	router: PropTypes.object
};

function getArticleById(articles, id) {
	const article = articles.filter(article => article.id == id);

	if(article.length) {
		return article[0];
	}

	return null;
}

function mapStateToProps(state, ownProps) {

	const articleID = ownProps.params.id;

	let article = {
		id		: "",
		title	: "",
		slug	: "",
		body	: ""
	};

	if(articleID && state.articles.length > 0) {
		article = getArticleById(state.articles, articleID);
	}

	return {
		article: article
	};

}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(ArticleActions, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageArticlePage);
