import React, { PropTypes } from "react";
import { Link } from "react-router";

const ArticlesListRow = ({ article }) => {

	return (
		<tr>
			<td><Link to={"/kmb/article/" + article.id}>{article.title}</Link></td>
			<td>{article.slug}</td>
		</tr>
	);

};

ArticlesListRow.propTypes = {
	article: PropTypes.object.isRequired
};

export default ArticlesListRow;
