import React, { PropTypes } from "react";

import Navbar from "./common/navbar.jsx";

class App extends React.Component {
	render() {
		return (
			<div className="container">
				<Navbar />
				{this.props.children}
			</div>
		);
	}
}

App.propTypes = {
	children: PropTypes.object.isRequired
};

export default App;
