import React from "react";
import TextInput from "../common/textInput.jsx";

const UserForm = ({user, onSave, onChange, saving, errors}) => {
	return (
		<form onSubmit={onSave}>
			
			<TextInput
				name="name"
				label="Nimi"
				value={user.name}
				onChange={onChange}
				error={errors.name}
			/>

			<TextInput
				name="email"
				label="Sähköposti"
				value={user.email}
				onChange={onChange}
				error={errors.email}
			/>

			<div className="form-group">
				<button type="submit" className="btn btn-success">
					Tallenna {saving ? <i className="fa fa-spinner fa-pulse fa-fw"></i> : ""}
				</button>
			</div>
			
		</form>
	);
};

UserForm.propTypes = {
	user: 		React.PropTypes.object.isRequired,
	onSave: 	React.PropTypes.func.isRequired,
	onChange: 	React.PropTypes.func.isRequired,
	saving: 	React.PropTypes.bool,
	errors: 	React.PropTypes.object
};

export default UserForm;
