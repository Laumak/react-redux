import React, { PropTypes } from "react";
import UserListRow from "./userListRow.jsx";

const UserList = ({ users }) => {

	return (
		<table className="table table-striped">

			<thead>
				<tr>
					<th>Nimi</th>
					<th>Email</th>
				</tr>
			</thead>
			
			<tbody>
				{users.map(user =>
					<UserListRow key={user.id} user={user} />
				)}
			</tbody>

		</table>
	);
};

UserList.propTypes = {
    users: PropTypes.array.isRequired
};

export default UserList;
