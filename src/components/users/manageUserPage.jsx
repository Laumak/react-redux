import React, { PropTypes } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { forEach } from "lodash";
import toastr from "toastr";

import * as userActions from "../../actions/userActions";
import UserForm from "./userForm.jsx";

class ManageUserPage extends React.Component {

	constructor(props, context) {
		super(props, context);

		this.state = {
			user	: Object.assign({}, props.user),
			errors	: {},
			saving	: false
		};

		this.updateUserState 	= this.updateUserState.bind(this);
		this.updateUser			= this.updateUser.bind(this);
		this.redirect 			= this.redirect.bind(this);
	}

	componentWillReceiveProps (nextProps) {
		if(this.props.user.id != nextProps.user.id) {
			return this.setState({user: Object.assign({}, nextProps.user)});
		}
	}
	

	updateUserState(event) {
		const field = event.target.name;
		let user = this.state.user;
		user[field] = event.target.value;

		return this.setState({ user: user });
	}

	updateUser(event) {
		event.preventDefault();

		this.setState({ saving: true });

		this.props.actions.updateUser(this.state.user)
			.then(() => {
				this.setState({ saving: false });

				this.redirect();
			})
			.catch((error) => {
				toastr.error("Käyttäjän tallennus epäonnistui");

				this.setState({ saving: false });
			});
	}

	redirect() {
		this.context.router.push("/kmb/users");
	}

	render() {
		return(
			<div className="panel panel-default">
				<div className="panel-heading">Muokkaa käyttäjää</div>
				<div className="panel-body">
				
					<UserForm
						user={this.state.user}
						onSave={this.updateUser}
						onChange={this.updateUserState}
						saving={this.state.saving}
						errors={this.state.errors}
					/>

				</div>
			</div>
		);
	}
	
}

ManageUserPage.propTypes = {
	user: PropTypes.object.isRequired,
	actions: PropTypes.object.isRequired
};

ManageUserPage.contextTypes = {
	router: PropTypes.object
};

function getUserById(users, id) {
	const user = users.filter(user => user.id == id);

	if(user.length) {
		return user[0];
	}

	return null;
}

function mapStateToProps(state, ownProps) {

	const userID = ownProps.params.id;

	let user = {
		id			: "",
		name		: "",
		email		: "",
		created_at	: "",
		updated_at	: "",
		last_login	: ""
	};

	if(userID && state.users.length > 0) {
		user = getUserById(state.users, userID);
	}

	return {
		user: user
	};

}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(userActions, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageUserPage);
