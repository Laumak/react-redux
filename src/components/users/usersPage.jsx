import React, { PropTypes } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { browserHistory } from "react-router";

import { orderBy } from "lodash";

import * as UserActions from "../../actions/userActions";
import UserList from "./userList.jsx";

class UsersPage extends React.Component {

	constructor(props, context) {
		super(props, context);

		// this.redirectToAddCoursePage = this.redirectToAddCoursePage.bind(this);
	}

	userRow(user, index) {
		return <div key={index}>{user.title}</div>;
	}

	render() {
		const users = this.props.users;
		const sortedUsers = orderBy(users, ["updated_at", "name"], ["desc", "asc"]);

		return (
			<div>

				<div className="panel panel-default">
					<div className="panel-heading">Käyttäjät</div>
					<div className="panel-body clearfix">

						<UserList users={sortedUsers} />

					</div>
				</div>

			</div>
		);
	}

}

UsersPage.propTypes = {
	users: PropTypes.array.isRequired,
	actions: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
	return {
		// Ks. RootReducerin (reducers/index.js) combineReducers funktion sisältö.
		users: state.users
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(UserActions, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(UsersPage);
