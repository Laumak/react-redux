import React from "react";

import { Modal, Button } from 'react-bootstrap';

const LoginModal = ({onChange, onSubmit, creds, saving, showModal, close}) => {
    return(
		<div className="static-modal">
			<Modal show={showModal} onHide={close}>

				<Modal.Header closeButton>
					<Modal.Title>Kirjaudu</Modal.Title>
				</Modal.Header>

				<Modal.Body>

					<form className="form-horizontal" onSubmit={onSubmit}>
						<div className="form-group">
							<label htmlFor="email" className="control-label col-sm-2 col-sm-offset-2">Sähköposti</label>

							<div className="col-sm-6">
								<input
									type="text"
									value={creds.email}
									id="email"
									className="form-control"
									onChange={onChange}
								/>
							</div>

						</div>

						<div className="form-group">
							<label htmlFor="password" className="control-label col-sm-2 col-sm-offset-2">Salasana</label>

							<div className="col-sm-6">
								<input
									type="password"
									value={creds.password}
									id="password"
									className="form-control col-sm-10"
									onChange={onChange}
								/>
							</div>

						</div>

						
						<div className="row">
							<div className="col-sm-offset-4 col-sm-6">
								<button className="btn btn-success" type="submit">
									Kirjaudu {saving ? <i className="fa fa-spinner fa-pulse fa-fw"></i> : ""}
								</button>
							</div>
						</div>

					</form>
				
				</Modal.Body>

			</Modal>
		</div>
    );
};

LoginModal.propTypes = {
	onChange	: React.PropTypes.func.isRequired,
	onSubmit    : React.PropTypes.func.isRequired,
	creds		: React.PropTypes.object.isRequired,
	saving		: React.PropTypes.bool.isRequired,
	showModal	: React.PropTypes.bool.isRequired,
	close		: React.PropTypes.func.isRequired
};

export default LoginModal;
