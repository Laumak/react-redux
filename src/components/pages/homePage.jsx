import React from "react";
import { Link } from "react-router";

class HomePage extends React.Component {
	render() {
		return (
            <div id="homePage" className="col-md-12">
                <h3>Mits Admin</h3>

                <p>Kirjaudu sisään hallitaksesi sivustoja.</p>
            </div>
		);
	}
}

export default HomePage;
