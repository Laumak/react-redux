import initialState from "./initialState";
import * as types from "../actions/actionTypes";


export default function ArticleReducer(state = initialState.articles, action) {
	
	switch (action.type) {

		case types.GET_ARTICLES_SUCCESS: {
			return action.articles;
		}

		case types.CREATE_ARTICLE_SUCCESS: {
			return [
				...state, Object.assign({}, action.savedArticle)
			];
		}
		
		case types.UPDATE_ARTICLE_SUCCESS: {
			return [
				...state.filter(function(article) {
					return article.id !== action.savedArticle.id;
				}), Object.assign({}, action.savedArticle)
			];
		}

		default: {
			return state;
		}
			
	}
}
