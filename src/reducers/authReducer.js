import { browserHistory } from "react-router";

import toastr from "toastr";
import cookie from "react-cookie";

import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default function AuthReducer(state = initialState.auth, action) {

	let token = cookie.load("token");
	
	switch (action.type) {

		case types.AUTH_SUCCESS: {

			if(! token) {
				token = action.response.data.token;
				cookie.save("token", token, { path: "/", maxAge: 7200 });
			}

			toastr.success(action.response.message);
			
			let auth = {
				authenticated: true,
				user: action.response.data.user
			};

			return auth;

		}

		case types.AUTH_FAILURE: {
			toastr.error(action.response.message);

			return false;
		}

		case types.GET_AUTH_SUCCESS: {
			let auth = {
				authenticated: true,
				user: action.response.user
			};

			return auth;
		}

		case types.AUTH_LOGOUT_SUCCESS: {

			cookie.remove("token", { path: "/" });

			let auth = {
				authenticated: false,
				user: {}
			};

			toastr.success("Uloskirjautuminen onnistui.");

			browserHistory.push("/");

			return auth;
		}

		default: {
			return state;
		}
			
	}
}
