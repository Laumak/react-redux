import * as types from "../actions/actionTypes";
import initialState from "./initialState";

export default function UserReducer(state = initialState.users, action) {
	
	switch (action.type) {

		case types.GET_USERS_SUCCESS: {
			return action.users;
		}
		
		case types.UPDATE_USER_SUCCESS: {
			return [
				...state.filter(user => user.id !== action.user.id),
				Object.assign({}, action.user)
			];	
		}

		default:
			return state;
			
	}
}
