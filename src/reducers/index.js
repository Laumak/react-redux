import { combineReducers } from "redux";

import auth			from "./authReducer";
import users 		from "./userReducer";
import articles 	from "./articleReducer";

const rootReducer = combineReducers({
	auth 		: auth,
	users		: users,
	articles	: articles
});

export default rootReducer;
