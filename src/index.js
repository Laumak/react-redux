/*eslint-disable import/default */
import "babel-polyfill";
import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import { Router, browserHistory, Route, IndexRoute } from "react-router";

import App 					from "./components/app.jsx";
import Dashboard 			from "./components/pages/dashboard.jsx";
import HomePage 			from "./components/pages/homePage.jsx";
import UsersPage 			from "./components/users/usersPage.jsx";
import ManageUserPage 		from "./components/users/manageUserPage.jsx";
import ArticlesPage 		from "./components/articles/articlesPage.jsx";
import ManageArticlePage 	from "./components/articles/manageArticlePage.jsx";

import configureStore 	from "./store/configureStore";

import { getAuth }		from "./actions/authActions";
import { getUsers } 	from "./actions/userActions";
import { getArticles } 	from "./actions/articleActions";

import "./styles/app.sass";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "../node_modules/toastr/build/toastr.css";
import "../node_modules/font-awesome/css/font-awesome.css";


const store = configureStore();

store.dispatch(getAuth());
store.dispatch(getUsers());
store.dispatch(getArticles());

render(
	<Provider store={store}>
		<Router history={browserHistory}>

			<Route path="/" component={App}>
				<IndexRoute component={HomePage} />

				<Route path="/dashboard"		component={Dashboard} />
			</Route>

			<Route path="/kmb" component={App}>
				<Route path="/kmb/users" 		component={UsersPage} />
				<Route path="/kmb/user" 		component={ManageUserPage} />
				<Route path="/kmb/user/:id" 	component={ManageUserPage} />

				<Route path="/kmb/articles" 	component={ArticlesPage} />
				<Route path="/kmb/article" 		component={ManageArticlePage} />
				<Route path="/kmb/article/:id" 	component={ManageArticlePage} />
			</Route>

		</Router>
	</Provider>,

	document.getElementById("app")
);
