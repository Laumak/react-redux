import * as types from "./actionTypes";

import toastr from "toastr";
import cookie from "react-cookie";
import superagent from "superagent";

import * as api from "../config.js";

export function getArticlesSuccess(articles) {
    return { type: types.GET_ARTICLES_SUCCESS, articles };
}

export function updateArticleSuccess(savedArticle) {
    return { type: types.UPDATE_ARTICLE_SUCCESS, savedArticle };
}

export function createArticleSuccess(savedArticle) {
    return { type: types.CREATE_ARTICLE_SUCCESS, savedArticle };
}

export function getArticles() {

	const token = cookie.load("token");

	return function(dispatch, getState) {
		
		if(token) {
			return superagent
				.get(api.getArticles)
				.set("Authorization", "Bearer " + token)
				.end((error, response) => {

					if(! response.ok) {
						toastr.error(response.body.error);
					} else {
						let articles = response.body.data;

						dispatch(getArticlesSuccess(articles));
					}		
				});	
		}
	};

}

export function saveArticle(article) {

	return function(dispatch, getState) {
		
		let url = "";
		const token = cookie.load("token");
		const user = getState().auth.user;

		if(article.id) {
			url = api.createArticle + "/" + article.id;
		} else {
			url = api.createArticle;
		}

		return superagent
			.post(url)
			.set("Authorization", "Bearer " + token)
			.send({article, user})
			.then((response) => {

				let savedArticle = response.body.data;

				if(article.id) {
					dispatch(updateArticleSuccess(savedArticle));
					toastr.success(response.body.message);
				} else {
					dispatch(createArticleSuccess(savedArticle));
					toastr.success(response.body.message);
				}

			})
			.catch((error) => {
				toastr.error(error.response.body.error);
			});
	};

}