import toastr from "toastr";
import cookie from "react-cookie";
import superAgent from "superagent";

import * as api from "../config.js";
import * as types from "./actionTypes";

import * as users from "./userActions";
import * as articles from "./articleActions";

export function authSuccess(response) {
    return { type: types.AUTH_SUCCESS, response };
}

export function authFailure(response) {
    return { type: types.AUTH_FAILURE, response };
}

export function getAuthSuccess(response) {
    return { type: types.GET_AUTH_SUCCESS, response };
}

export function logoutSuccess() {
    return { type: types.AUTH_LOGOUT_SUCCESS };
}

export function authenticate(credentials) {

    return function(dispatch, getState) {
        return superAgent
            .post(api.login)
            .send(credentials)
            .then((success) => {
                let data = success.body;

                dispatch(authSuccess(data));
                dispatch(articles.getArticles());
                dispatch(users.getUsers());
            })
            .catch((error) => {
                let data = error.response.body;

                dispatch(authFailure(data));
            });
    };

}

export function getAuth() {

    return function(dispatch, getState) {

        const token = cookie.load("token");

        if(token) {
            return superAgent
                .get(api.checkAuth)
                .set("Authorization", "Bearer " + token)
                .then((success) => {
                    let data = success.body.data;

                    dispatch(getAuthSuccess(data));
                });
        }
    };

}

export function logout() {

    return function(dispatch, getState) {
        dispatch(logoutSuccess());
    };

}

