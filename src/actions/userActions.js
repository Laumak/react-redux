import * as types from "./actionTypes";

import toastr from "toastr";
import cookie from "react-cookie";
import superagent from "superagent";

import * as api from "../config.js";

export function getUsersSuccess(users) {
    return { type: types.GET_USERS_SUCCESS, users };
}

export function updateUserSuccess(user) {
    return { type: types.UPDATE_USER_SUCCESS, user };
}

export function getUsers() {

	const token = cookie.load("token");

	return function(dispatch) {
		
		if(token) {
			return superagent
				.get(api.getUsers)
				.set("Authorization", "Bearer " + token)
				.end((error, response) => {

					if(! response.ok) {
						toastr.error(response.body.error);
					} else {
						let users = response.body.data.users;

						dispatch(getUsersSuccess(users));
					}		
				});
		}
	};

}

export function updateUser(user) {

	const token = cookie.load("token");

	return function(dispatch, getState) {

		return superagent
			.post(api.updateUser + "/" + user.id)
			.set("Authorization", "Bearer " + token)
			.send({
				user: user
			})
			.then((response) => {
				const savedUser = response.body.data;
				
				dispatch(updateUserSuccess(savedUser));
				toastr.success(response.body.message);
			})
			.catch((error) => {
				toastr.error(error.response.body.error);
			});
	};

}