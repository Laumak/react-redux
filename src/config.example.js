export const url = "http://mikrobiologit.dev/api/v0/";

export const login      = url + "login";
export const checkAuth  = url + "getUser";

export const getUsers   = url + "users";
export const updateUser = url + "user";

export const getArticles    = url + "articles";
export const createArticle  = url + "article";