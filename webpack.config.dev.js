import webpack from "webpack";
import path from "path";

export default {
	debug: true,
	devtool: "inline-source-map",
	noInfo: false,
	resolve: {
		extensions: ["", ".webpack.js", ".web.js", ".js", ".jsx"]
	},
	entry: [
		"eventsource-polyfill", // necessary for hot reloading with IE
		"webpack-hot-middleware/client?reload=true", //note that it reloads the page if hot module reloading fails.
		"./src/index"
	],
	target: "web",
	output: {
		path: __dirname + "/dist", // Note: Physical files are only output by the production build task `npm run build`.
		publicPath: "/",
		filename: "bundle.js"
	},
	devServer: {
		contentBase: "./src"
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoErrorsPlugin(),
		new webpack.ProvidePlugin({
			$: "jquery",
			jQuery: "jquery"
		})
	],
	module: {
		loaders: [
			{
				test: /\.jsx?$/,
				loaders: ["babel"],
				include: path.join(__dirname, "src")
			},
			{
				test: /\.sass$/,
				loaders: ["style", "css?sourceMap", "sass?sourceMap"],
				include: path.join(__dirname, "src/styles")
			},
			{
				test: /(\.css)$/,
				loaders: ["style", "css"]
			},
			{
				test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
				loader: "url?limit=10000&mimetype=application/font-woff"
			}, 
			{
				test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
				loader: "url?limit=10000&mimetype=application/font-woff"
			}, 
			{
				test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
				loader: "url?limit=10000&mimetype=application/octet-stream"
			}, 
			{
				test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
				loader: "file"
			}, 
			{
				test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
				loader: "url?limit=10000&mimetype=image/svg+xml"
			}
		]
	}
};
